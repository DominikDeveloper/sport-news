Django==2.1.4
django-ckeditor==5.6.1
django-colorful==1.3
django-contact-form==1.6
django-easy-poll==0.0.1
django-photologue==3.8.1
django-sitetree==1.11.0
django-taggit==0.23.0
