from django.http import HttpResponse
from django.core import serializers
from django.core.paginator import EmptyPage, InvalidPage, Paginator
from django.shortcuts import render
from django.views.generic.dates import MonthArchiveView

from .models import Event, Entry

from news_portal.utilities import get_layout_dict, render_with_db_extra_context

# Create your views here.
def relation_list(request):
    records = Event.objects.select_related().filter(is_published = True)

    paginator = Paginator(records, 10)

    page = request.GET.get('page')
    try:
        events = paginator.page(page)
    except (EmptyPage, InvalidPage):
        events = paginator.page(1)


    return render_with_db_extra_context(request, 'liveupdate/relation_list.html', {
        'object_list': events
        })

def update_event_list(request, event_id):
    records = Entry.objects.select_related().filter(event_id = event_id)
    events = Event.objects.select_related().filter(pk = event_id, is_published = True)
    if (events):
        event = events[0]
        return render_with_db_extra_context(request, 'liveupdate/update_list.html', {
            'object_list': records,
            'event_name': event.name,
            'event_id': event.pk,
            'event_ended': '[ended]' if (event.is_ended) else '',
            })
    return HttpResponse(status=404)

def update_after(request, update_id):
    response = HttpResponse()
    response['Content-Type'] = "text/javascript"
    response.write(
        serializers.serialize(
            "json", Entry.objects.select_related().filter(pk__gt = update_id)
            )
        )

    return response

class EventMonthArchiveView(MonthArchiveView):
    queryset = Event.objects.select_related().filter(is_published = True, is_ended = False)
    date_field = 'start_time'
    allow_future = True
    extra_context = get_layout_dict()