from django.contrib import admin
from .models import Event, Entry

def make_published(modeladmin, request, queryset):
    queryset.update(is_published=True)
make_published.short_description = "Mark selected stories as published"

def make_hidden(modeladmin, request, queryset):
    queryset.update(is_published=False)
make_hidden.short_description = "Mark selected stories as hidden"

def make_ended(modeladmin, request, queryset):
    queryset.update(is_ended=True)
make_ended.short_description = "Set selected stories as ended"

def make_ongoing(modeladmin, request, queryset):
    queryset.update(is_ended=False)
make_ongoing.short_description = "Set selected stories as ongoing"

class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'start_time', 'is_published', 'is_ended')
    list_filter = ('start_time', 'is_published', 'is_ended')
    search_fields = ['name']
    date_hierarchy = 'start_time'
    ordering = ['-start_time', '-is_published']
    actions = [make_published, make_hidden, make_ended, make_ongoing]

class EntryAdmin(admin.ModelAdmin):
    list_display = ('event_id', 'text', 'timestamp')
    list_filter = ('event_id', 'timestamp')
    search_fields = ['event_id', 'text']
    date_hierarchy = 'timestamp'
    ordering = ['-timestamp', '-event_id', 'text']

# Register your models here.
admin.site.register(Event, EventAdmin)
admin.site.register(Entry, EntryAdmin)