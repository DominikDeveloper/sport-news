from .models import Event

from django.utils import timezone
import pytz

def events(request):
    
    db_events = Event.objects.select_related().filter(is_published = True, is_ended = False).order_by('-start_time')
    is_utc = (timezone.get_current_timezone() == pytz.timezone("UTC"))
    # set timezone for Events datetime
    if (is_utc == False):
        conv_events = []
        for item in db_events:
            utc_date = item.start_time
            tz_date = timezone.localtime(utc_date)
            conv_events.append({'start_time': tz_date})

    return {
        'ongoing_live_events': db_events if (is_utc) else conv_events
    }
