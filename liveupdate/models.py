from django.db import models
from django.contrib import admin
from django.urls import reverse

from django.utils import timezone

from datetime import date, datetime


# Create your models here.
class Event(models.Model):
    name = models.CharField(max_length=250)
    is_published = models.BooleanField(default=True)
    start_time = models.DateTimeField(default=timezone.now)
    is_ended = models.BooleanField(default=False)

    class Meta:
        ordering = ['is_ended', '-start_time', '-id']

    def __str__(self):
        return self.name

    objects = models.Manager()
    

class Entry(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    event_id = models.ForeignKey(
        Event, on_delete = models.CASCADE
    )
    text = models.TextField()

    class Meta:
        ordering = ['-id']
        verbose_name_plural = "Entries"

    def __str__(self):
        timest = timezone.localtime(self.timestamp)
        string = timest.strftime("%Y-%m-%d %H:%M:%S") + " " + self.text
        return string

    objects = models.Manager()