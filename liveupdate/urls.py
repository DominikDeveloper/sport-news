from django.urls import path, re_path

from . import views

app_name = 'liveupdate'
urlpatterns = [
    re_path(r'^$', views.relation_list, name='event_list'),
    re_path(r'^live/(?P<event_id>\d+)/$', views.update_event_list, name='event_live_id'),
    re_path(r'^updates-after/(?P<update_id>\d+)/$', views.update_after),
    path('<int:year>/<int:month>/', views.EventMonthArchiveView.as_view(month_format='%m'), name='event_archive_month'),
]