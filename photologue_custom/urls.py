from django.conf.urls import url
from django.contrib import admin
from django.urls import path, re_path, reverse_lazy
from django.views.generic import RedirectView

from photologue.views import PhotoListView, PhotoDetailView, GalleryListView, \
    GalleryDetailView, PhotoArchiveIndexView, PhotoDateDetailView, PhotoDayArchiveView, \
    PhotoYearArchiveView, PhotoMonthArchiveView, GalleryArchiveIndexView, GalleryYearArchiveView, \
    GalleryDateDetailView, GalleryDayArchiveView, GalleryMonthArchiveView, GalleryDateDetailOldView, \
    GalleryDayArchiveOldView, GalleryMonthArchiveOldView, PhotoDateDetailOldView, \
    PhotoDayArchiveOldView, PhotoMonthArchiveOldView

from news_portal.utilities import get_layout_dict

# app_name = 'photologue'
urlpatterns = [
    url(r'^gallery/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/(?P<slug>[\-\d\w]+)/$',
        GalleryDateDetailView.as_view(month_format='%m', extra_context = get_layout_dict()),
        name='gallery-detail'),
    url(r'^gallery/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/$',
        GalleryDayArchiveView.as_view(month_format='%m', extra_context = get_layout_dict()),
        name='gallery-archive-day'),
    url(r'^gallery/(?P<year>\d{4})/(?P<month>[0-9]{2})/$',
        GalleryMonthArchiveView.as_view(month_format='%m', extra_context = get_layout_dict()),
        name='gallery-archive-month'),
    url(r'^gallery/(?P<year>\d{4})/$',
        GalleryYearArchiveView.as_view(extra_context = get_layout_dict()),
        name='pl-gallery-archive-year'),
    url(r'^gallery/$',
        GalleryArchiveIndexView.as_view(extra_context = get_layout_dict()),
        name='pl-gallery-archive'),
    url(r'^$',
        RedirectView.as_view(
            url=reverse_lazy('photologue:pl-gallery-archive'), permanent=True),
        name='pl-photologue-root'),
    url(r'^gallery/(?P<slug>[\-\d\w]+)/$',
        GalleryDetailView.as_view(extra_context = get_layout_dict()), name='pl-gallery'),
    url(r'^gallerylist/$',
        GalleryListView.as_view(extra_context = get_layout_dict()),
        name='gallery-list'),

    url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/(?P<slug>[\-\d\w]+)/$',
        PhotoDateDetailView.as_view(month_format='%m', extra_context = get_layout_dict()),
        name='photo-detail'),
    url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/$',
        PhotoDayArchiveView.as_view(month_format='%m', extra_context = get_layout_dict()),
        name='photo-archive-day'),
    url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/$',
        PhotoMonthArchiveView.as_view(month_format='%m', extra_context = get_layout_dict()),
        name='photo-archive-month'),
    url(r'^photo/(?P<year>\d{4})/$',
        PhotoYearArchiveView.as_view(extra_context = get_layout_dict()),
        name='pl-photo-archive-year'),
    url(r'^photo/$',
        PhotoArchiveIndexView.as_view(extra_context = get_layout_dict()),
        name='pl-photo-archive'),

    url(r'^photo/(?P<slug>[\-\d\w]+)/$',
        PhotoDetailView.as_view(extra_context = get_layout_dict()),
        name='pl-photo'),
    url(r'^photolist/$',
        PhotoListView.as_view(extra_context = get_layout_dict()),
        name='photo-list'),
]

