from django import template

from django.template.loader import render_to_string
from django.shortcuts import render

from news_portal.models import StaticPage

register = template.Library()


@register.simple_tag(takes_context=True)
def pagetree(context):
    
    p = StaticPage.published.all().only('label', 'status', 'url')
    

    content = render_to_string('news_portal/navigation.html', {
        'pages': p,
    })
    
    return content