from django import template
from django.template.loader import render_to_string
from django.utils.html import format_html

from poll.models import Item, Poll

from random import randint

register = template.Library()


@register.simple_tag(takes_context=True)
def poll(context):
    request = context['request']

    try:
        polls = Poll.objects.select_related().filter(is_published = True)
        poll_numbers = polls.count()

        if (poll_numbers > 1):
            poll = polls[randint(0, poll_numbers - 1)]
        elif (poll_numbers == 1):
            poll = polls[0]
        else:
            raise ValueError('Any poll detected')
    except:
        return ''

    items = Item.objects.filter(poll=poll)

    if poll.get_cookie_name() in request.COOKIES:
        template = "poll/result.html"
    else:
        template = "poll/poll.html"

    content = render_to_string(template, {
        'poll': poll,
        'items': items,
    })

    return content
