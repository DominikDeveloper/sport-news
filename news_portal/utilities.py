from django.shortcuts import render
from .models import Color, Label

def get_colors():
    try:
        c = Color.objects.all().order_by('pk')
    except:
        c = []
    return c

def get_labels():
    try:
        l = Label.objects.filter()[:1].get()
    except:
        l = []
    return l

def get_layout_dict():
    
    colors = get_colors()
    labels = get_labels()

    if (labels == [] or colors == []):
        return {}
    
    return {
        'content_bg': colors[2].rgb if (colors.count() >= 3 and colors[2].is_active) else '',
        'content_txt': colors[3].rgb if (colors.count() >= 4 and colors[3].is_active) else '',
        'footer_bg': colors[5].rgb if (colors.count() >= 6 and colors[5].is_active) else '',
        'footer_txt': colors[6].rgb if (colors.count() >= 7 and colors[6].is_active) else '',
        'header_bg': colors[0].rgb if (colors.count() >= 1 and colors[0].is_active) else '',
        'header_txt': colors[1].rgb if (colors.count() >= 2 and colors[1].is_active) else '',
        'page_bg': colors[4].rgb  if (colors.count() >= 5 and colors[4].is_active) else '',

        'label': labels,
    }

def render_with_db_extra_context(request, template, content):  
    
    extra_dict = get_layout_dict()

    content_with_colors = {**content, **extra_dict} 

    return render(request, template, content_with_colors)