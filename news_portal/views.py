from django.http import HttpResponse
from django.shortcuts import render, reverse, get_object_or_404
from .models import Article, Color, Label, StaticPage

from news_portal.utilities import get_colors, get_labels, get_layout_dict, render_with_db_extra_context

from liveupdate.views import relation_list

from django.core.paginator import EmptyPage, InvalidPage, Paginator

from django.views.generic.dates import MonthArchiveView
from django.views.generic.list import ListView

from taggit.models import Tag

# Create your views here.
def article_list(request):
    articles_list = Article.published.all()

    paginator = Paginator(articles_list, 5)

    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except (EmptyPage, InvalidPage):
        articles = paginator.page(1)
    

    return render_with_db_extra_context(request, 'news_portal/article/list.html', {
        'articles': articles, 
        })

def article_detail(request, year, month, day, slug):
    
    if (request.user.is_authenticated):
        article = get_object_or_404(Article, slug=slug,
        publish__year = year,
        publish__month = month,
        publish__day = day)
    else:
        article = get_object_or_404(Article, slug=slug,
        status = 'published',
        publish__year = year,
        publish__month = month,
        publish__day = day)
        
    return render_with_db_extra_context(request, 'news_portal/article/detail.html', {
        'article': article, 
        })

def static_page(request, slug, template=u"news_portal/content.html"):

    p = get_object_or_404(StaticPage.published, url=slug)

    return render_with_db_extra_context(request, template, {
        'page_content': p.content, 
        })

class ArticleMonthArchiveView(MonthArchiveView):
    queryset = Article.published.all()
    date_field = 'publish'
    allow_future = True
    extra_context = get_layout_dict()

class TagMixin(object):
    def get_context_data(self, **kwargs):
        context = super(TagMixin, self).get_context_data(**kwargs)
        context['tags'] = Tag.objects.all()
        return context

class TagIndexView(TagMixin, ListView):
    template_name='news_portal/article_list_by_tag.html'
    model = Article
    context_object_name = 'article_list_by_tag'#articles
    extra_context = get_layout_dict()

    def get_queryset(self):
        return Article.published.filter(tags__slug=self.kwargs.get('slug')).order_by('-publish') #.all().first()
