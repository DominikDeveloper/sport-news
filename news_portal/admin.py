from django.contrib import admin

from django.utils.safestring import mark_safe 

from .models import Article, AuditEntry, Color, Label, StaticPage

# Helpers
def make_published(modeladmin, request, queryset):
    queryset.update(status='published')
make_published.short_description = "Mark selected stories as published"

def make_hidden(modeladmin, request, queryset):
    queryset.update(status='draft')
make_hidden.short_description = "Mark selected stories as draft"

class ViewOnSite(object):
    def expanded_view_on_site(self, obj):
        return mark_safe(
            u"<a href='{0}' target='_blank'>{1}</a>".format(
                obj.get_absolute_url(),
                ("Link")
            )
            )
    expanded_view_on_site.allow_tags = True
    expanded_view_on_site.short_description = u"View on site"

# ModelAdmins
class ArticleAdmin(ViewOnSite, admin.ModelAdmin):
    list_display = ('title', 'expanded_view_on_site', 'slug', 'author', 'publish', 'status', 'tag_list')
    list_filter = ('status', 'created', 'publish', 'author')
    search_fields = ('title', 'body')
    prepopulated_fields = {"slug": ("title",)}
    date_hierarchy = 'publish'
    ordering = ['status', 'publish']
    actions = [make_published, make_hidden]

    exclude = ('author',)

    def view_on_site(self, obj):
        return obj.get_absolute_url()

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        super().save_model(request, obj, form, change)

    def get_queryset(self, request):
        return super(ArticleAdmin, self).get_queryset(request).prefetch_related('tags')

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())

class ColorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'rgb', 'is_active',)
    list_filter = ['name']
    search_fields = ['name', 'id']
    ordering = ['id']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class StaticPageAdmin(admin.ModelAdmin):
    list_display = ('label', 'url', 'parent_id', 'status',)
    list_filter = ['status', 'created_at', 'updated_at']
    search_fields = ['label', 'content']
    prepopulated_fields = {"url": ("label",)}
    ordering = ['-updated_at', '-status']
    actions = [make_published, make_hidden]

class LabelAdmin(admin.ModelAdmin):
    list_display = ('tab_title', 'header_text', 'footer_text',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
    
@admin.register(AuditEntry)
class AuditEntryAdmin(admin.ModelAdmin):
    list_display = ['action', 'username', 'ip', 'timestamp',]
    list_filter = ['action', 'username', 'ip', 'timestamp',]
    ordering = ['-timestamp']
    readonly_fields = ['action', 'username', 'ip',]

    def has_add_permission(self, request):
        return False

# Register your models here.
admin.site.register(Article, ArticleAdmin)
admin.site.register(Color, ColorAdmin)
admin.site.register(StaticPage, StaticPageAdmin)
admin.site.register(Label, LabelAdmin)