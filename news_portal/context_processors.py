from .models import Article
from taggit.models import Tag
from django.utils import timezone
import pytz

def articles(request):
    
    db_articles = Article.published.order_by('-publish')
    is_utc = (timezone.get_current_timezone() == pytz.timezone("UTC"))
    # set timezone for Articles datetime
    if (is_utc == False):
        conv_articles = []
        for item in db_articles:
            utc_date = item.publish
            tz_date = timezone.localtime(utc_date)
            conv_articles.append({'publish': tz_date})
    
    return {
        'all_articles': db_articles if (is_utc) else conv_articles,
        'tag_list': Tag.objects.all()
    }