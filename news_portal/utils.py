from django.utils import timezone

def get_image_path(instance, filename):
    now = timezone.now()
    year = "%d" % now.year
    month = "%d" % now.month
    day = "%d" % now.day

    return "photos/%s/%s/%s/%s" % (str(year), str(month), str(day), filename)
