from django.urls import path, re_path
from django.conf import settings
from django.urls import include #

from . import views

app_name = 'news_portal'
urlpatterns = [
    path('', views.article_list, name='article_list'),
    re_path(r'^article/(?P<year>\d+)/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[\w-]+)/$', views.article_detail, name='article_detail'),
    path('article/archive/<int:year>/<int:month>/', views.ArticleMonthArchiveView.as_view(month_format='%m'), name='post_archive_month'),
    
    re_path(r'^tag/(?P<slug>[-\w]+)/$', views.TagIndexView.as_view(), name='tagged'),
    re_path("^page/(?P<slug>.*)%s$" % ("/" if settings.APPEND_SLASH else ""), views.static_page, name="static_page"),
]