from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
from news_portal.utils import get_image_path

from django.contrib.auth.signals import user_logged_in, user_logged_out, user_login_failed
from django.dispatch import receiver

from datetime import date, datetime
from django.utils import timezone

from ckeditor.fields import RichTextField

from colorful.fields import RGBColorField

from taggit.managers import TaggableManager

# Create your models here.
class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset().filter(status='published')

class Article(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, unique_for_date='publish')
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='news_portal_articles')
    body = RichTextField()
    cover_image = models.ImageField(upload_to=get_image_path, max_length=255, null=True, blank=True)
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(
        max_length=25, choices=STATUS_CHOICES, default='draft')
    tags = TaggableManager(blank=True)

    class Meta:
        ordering = ('-publish',)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        pub_date = timezone.localtime(self.publish)
        return reverse('news_portal:article_detail', args=[
            pub_date.year,
            pub_date.strftime('%m'),
            pub_date.strftime('%d'),
            self.slug
        ])

    objects = models.Manager()
    published = PublishedManager()

class Color(models.Model):
    name = models.CharField(max_length=250)
    rgb = RGBColorField()
    is_active = models.BooleanField(default=False)

    objects = models.Manager()

    def __str__(self):
        return self.name

class StaticPage(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )
    
    parent_id = models.ForeignKey(
        "self", on_delete=models.CASCADE, blank = True, null=True)
    label = models.CharField(max_length=250)
    status = models.CharField(
        max_length=25, choices=STATUS_CHOICES, default='draft')

    content = RichTextField(blank=True)

    metatitle = models.CharField(max_length=70, blank=True)
    url = models.SlugField(max_length=250, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = models.Manager()
    published = PublishedManager()

    def __str__(self):
        return self.label

class Label(models.Model):
    tab_title = models.CharField(max_length=70, blank=True)
    header_text = models.CharField(max_length=100)
    footer_text = models.TextField(blank=True)

    objects = models.Manager()

class AuditEntry(models.Model):
    action = models.CharField(max_length=64)
    ip = models.GenericIPAddressField(null=True)
    username = models.CharField(max_length=256, null=True)
    timestamp = models.DateTimeField(auto_now=True)

    objects = models.Manager()

    class Meta:
        verbose_name_plural = "Audit entries"

    def __str__(self):
        return '{0} - {1} - {2}'.format(self.action, self.username, self.ip)


@receiver(user_logged_in)
def user_logged_in_callback(sender, request, user, **kwargs):  
    ip = request.META.get('REMOTE_ADDR')
    AuditEntry.objects.create(action='user_logged_in', ip=ip, username=user.username)


@receiver(user_logged_out)
def user_logged_out_callback(sender, request, user, **kwargs):  
    ip = request.META.get('REMOTE_ADDR')
    AuditEntry.objects.create(action='user_logged_out', ip=ip, username=user.username)


@receiver(user_login_failed)
def user_login_failed_callback(sender, credentials, **kwargs):
    AuditEntry.objects.create(action='user_login_failed', username=credentials.get('username', None))
