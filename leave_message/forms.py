from django import forms

class MessageForm(forms.Form):
    from_email = forms.EmailField(required=True)
    subject = forms.CharField(required=True, max_length=250)
    content = forms.CharField(widget=forms.Textarea, required=True, max_length=1000)