from django.contrib import admin
from .models import Message

def make_read(modeladmin, request, queryset):
    queryset.update(is_read=True)
make_read.short_description = "Mark selected emails as read"

def make_unread(modeladmin, request, queryset):
    queryset.update(is_read=False)
make_unread.short_description = "Mark selected emails as unread"

class MessageAdmin(admin.ModelAdmin):
    readonly_fields = ['subject', 'from_email', 'content', 'timestamp']
    list_display = ('subject', 'is_read', 'from_email', 'content', 'timestamp')
    list_filter = ('is_read', 'timestamp', 'from_email',)
    search_fields = ['subject', 'content', 'from_email']
    date_hierarchy = 'timestamp'
    ordering = ['-timestamp', 'subject', 'content', 'from_email']
    actions = [make_read, make_unread]

    def has_add_permission(self, request):
        return False

# Register your models here.
admin.site.register(Message, MessageAdmin)