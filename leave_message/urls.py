from django.contrib import admin
from django.urls import path, re_path

from . import views

app_name = 'leave_message'
urlpatterns = [
    path('email/', views.emailView, name='email'),
    path('success/', views.successView, name='success'),
    path('error/', views.errorView, name='error'),
]