from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from .forms import MessageForm
from .models import Message

from news_portal.utilities import render_with_db_extra_context

# Helpers
def get_form_page(request, form):
    return render_with_db_extra_context(request, 'leave_message/email.html', {
        'form': form,})

def redirect_to_ok_alert_page(request):
    return HttpResponseRedirect(
        reverse('leave_message:success')
        )

def redirect_to_err_alert_page(request):
    return HttpResponseRedirect(
        reverse('leave_message:error')
        )

# Create your views here.
def emailView(request):
    if request.method == 'GET':
        form = MessageForm()
    else:
        form = MessageForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            content = form.cleaned_data['content']
            try:
                send_mail(subject, content, from_email, ['admin@example.com'])
                f = Message(
                    from_email=from_email,
                    subject=subject,
                    content=content)
                f.save()
            except BadHeaderError:
                return redirect_to_err_alert_page(request)
            return redirect_to_ok_alert_page(request)
    return get_form_page(request, form)

def successView(request):
    return render_with_db_extra_context(request, 'leave_message/alert.html', {
        'status': 'ok',
        'alerttext': 'Well done! You successfully sent message.',})\

def errorView(request):
    return render_with_db_extra_context(request, 'leave_message/alert.html', {
        'status': 'error',
        'alerttext': 'Oh no! It is possible that something needs to be improved.',})
