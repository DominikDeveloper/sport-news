from django.db import models
from django.utils.timezone import now

from datetime import date, datetime

class Message(models.Model): 
    from_email = models.EmailField()
    subject = models.CharField(max_length=250)
    content = models.CharField(max_length=1000)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)

    class Meta:
        ordering = ['-timestamp']
        
    def __str__(self):
        timest = datetime.date(self.timestamp)
        string = "[" + timest.strftime("%Y-%m-%d %H:%M:%S") + "] " + self.subject
        return string