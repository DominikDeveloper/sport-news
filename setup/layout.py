from news_portal.models import Color, Label

## news_portal ##
color1 = Color(name='header_bg', rgb='#d8dbde', is_active=True)
color2 = Color(name='header_txt', rgb='#00ffff', is_active=False)
color3 = Color(name='content_bg', rgb='#ffffff', is_active=False)
color4 = Color(name='content_txt', rgb='#000000', is_active=False)
color5 = Color(name='page_bg', rgb='#eaeaea', is_active=False)
color6 = Color(name='footer_bg', rgb='#3a3a3a', is_active=True)
color7 = Color(name='footer_txt', rgb='#e1e1e1', is_active=True)

color1.save(force_insert=True)
color2.save(force_insert=True)
color3.save(force_insert=True)
color4.save(force_insert=True)
color5.save(force_insert=True)
color6.save(force_insert=True)
color7.save(force_insert=True)

label = Label(tab_title='Sport Blog', header_text='SportBlog', footer_text='2019 SportBlog')

label.save(force_insert=True)


